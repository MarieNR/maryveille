<?php include "included/connexion_bdd.php";

?>
<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Veille Mary</title>

  <?php 
   include 'shared/head.php';
   ?>
</head>

<body>
<!-- -->
  <?php include 'shared/header.php'; ?>
  <?php include "included/function.php";?>

<div class="container">
  <div class="row">
  <?php 
        for ($i=0; $i < count($showVeille); $i++) { 
            ?>

            <div class="col-12 col-lg-4 p-4">
              <div class="card text-center">
                <div class="card-body">
                  <h5 class="card-title"><?php echo utf8_encode($showVeille[$i]['sujet']); ?></h5>
                  <p class="card-text"><?php echo $showVeille[$i]['date']; ?></p>
                  <p class="card-text">
                    <?php
                      $bla = $showVeille[$i]['synthese'];
                      $thisVeille = utf8_encode($bla);
                      echo $thisVeille;
                    ?>
                  </p>
                  <p class="card-text"><img src="<?php echo utf8_encode($showVeille[$i]['image']); ?>" alt="" width="100px" height="100px"></p>
                  <p class="card-text"><?php echo utf8_encode($showVeille[$i]['commentaire']); ?></p>
                  <a href="<?php echo $showVeille[$i]['lien']; ?>" class="btn btn-primary">Source</a>
                </div>
              </div>
            </div>
            
            <?php
        }
        ?>
  </div>
</div>

    <?php include 'shared/footer.php'; ?>
</body>

</html>
