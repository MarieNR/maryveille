<?php
    header('Content-Type: application/rss+xml');
    include "../included/connexion_bdd.php";
    include "../included/function.php";

  echo "<?xml version=\"1.0\" encoding=\"utf-8\""?>

?>

<rss version="2.0">
    <channel>
        <title>Flux RSS</title>
        <link></link>
        <description>Flux RSS - MaryVeille</description>

        <?php 
        for ($i=0; $i < sizeof($veilleRss);$i++) {
        ?>
        <item>
            <title><?php echo $veilleRss[$i]['sujet']; ?></title>
            <link><?php echo $veilleRss[$i]['lien']; ?></link>
            <description><?php echo (str_replace("’", "'", $veilleRss[$i]['synthese'])); ?></description>
            <comments><?php echo (str_replace("’", "'", $veilleRss[$i]['commentaire'])); ?></comments>
            <image><?php echo $veilleRss[$i]['image_url']; ?>
            </image>
        </item>
        <?php } ?>
    </channel>
</rss>