<?php 
    include "included/connexion_bdd.php";
    include "included/function.php";
    include "js/functions.js";
  ?> 

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

   <?php include 'shared/head.php'; ?>

  
</head>
<body>
    <?php include 'shared/header.php'; ?>
    <div class="row">
      <div class="col-8">
        <div class="row">
          <div class="col-12">
            <div id= "carousselLast" class="card text-center">
              
            </div>
          </div>
        </div>

    <?php include 'shared/footer.php'; ?>   
    <script>
        var str = document.location.href;
        var url = new URL(str);
        var id = url.searchParams.get("id");
        console.log("id", id); 

        function getCarousselArticles(){

      base('Veille').find(id, function(err, record) {
        if (err) { console.error(err); return; }
        console.log('Article', record);
        document.getElementById('dateArticle4').innerHTML = record.get('Date');
        
        $('#carousselLast').append('<div id= "dateArticle4" class="card-header">' +record.get('Sujet')+'</div>'+ 
         '<div class="card-body">'+'<h5 class="card-title">'+ record.get ("Sujet")+' </h5>'+
         '<p class="card-text">'+ record.get ("Synthèse")+'.</p>'+ 
         '<p class="card-text">'+ record.get ("Commentaire")+'.</p>'+
         '<a href="#" class="btn btn-primary">'+ record.get ("Liens") +'</a>'+ 
         +'</div>'+ record.get("Image") +'<div class="card-footer text-muted">'+'2 days ago'+'</div>');
        });

    }

   </script>
</body>
</html>